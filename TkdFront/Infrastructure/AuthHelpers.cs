﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TkdFront.Infrastructure;

namespace TkdFront.Infrastructure
{
    public static class AuthConstants
    {
        public static readonly string IsSuper = nameof(IsSuper);
        public static readonly string IsAdmin = nameof(IsAdmin);
        public static readonly string IsUser = nameof(IsUser);
        public static readonly string TrueValue = "true";
    }
}

namespace System.Security.Claims
{
    public static class AuthnHelpers
    {
        // Super user
        public static bool IsSuper(this ClaimsPrincipal principal) =>
            principal.HasClaim(AuthConstants.IsSuper, AuthConstants.TrueValue);

        public static void MakeSuper(this ClaimsPrincipal principal) =>
            principal.Identities.First().MakeSuper();
        public static void MakeSuper(this ClaimsIdentity identity) =>
            identity.AddClaim(new Claim(AuthConstants.IsSuper, AuthConstants.TrueValue));
        // Admin User
        public static bool IsAdmin(this ClaimsPrincipal principal) =>
            principal.HasClaim(AuthConstants.IsAdmin, AuthConstants.TrueValue);

        public static void MakeAdmin(this ClaimsPrincipal principal) =>
            principal.Identities.First().MakeAdmin();
        public static void MakeAdmin(this ClaimsIdentity identity) =>
            identity.AddClaim(new Claim(AuthConstants.IsAdmin, AuthConstants.TrueValue));

        // Normal user
        public static bool IsUser(this ClaimsPrincipal principal) =>
            principal.HasClaim(AuthConstants.IsUser, AuthConstants.TrueValue);

        public static void MakeUser(this ClaimsPrincipal principal) =>
            principal.Identities.First().MakeUser();

        public static void MakeUser(this ClaimsIdentity identity) =>
            identity.AddClaim(new Claim(AuthConstants.IsUser, AuthConstants.TrueValue));
    }
}

namespace Microsoft.Extensions.DependencyInjection
{
    public static class AuthzHelpers
    {
        public static AuthorizationPolicyBuilder RequireIsAdminClaim(this AuthorizationPolicyBuilder builder) =>
            builder.RequireClaim(AuthConstants.IsAdmin, AuthConstants.TrueValue);
        
        public static AuthorizationPolicyBuilder RequireIsSuperClaim(this AuthorizationPolicyBuilder builder) =>
            builder.RequireClaim(AuthConstants.IsSuper, AuthConstants.TrueValue);
    }
}
