using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using TkdDTO;
using TkdFront.Services;
using System.Security.Claims;

namespace TkdFront.Pages
{
    public class TechniqueModel : PageModel
    {
        protected readonly IApiClient _apiClient;

        public TechniqueModel(IApiClient apiClient)
        {
            _apiClient = apiClient;
        }

        public TechniqueResponse Technique { get; set; }
        public List<ImageResponse> Images { get; set; }

        public bool IsAdmin { get; set; }
        public async Task OnGetAsync(int id)
        {
            IsAdmin = User.IsAdmin();
            Technique = await _apiClient.GetTechniqueAsync(id);
            Images = await _apiClient.GetImagesAsync(Technique.Wid);
        }
    }
}
