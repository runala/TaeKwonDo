using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using TkdDTO;
using TkdFront.Services;
using System.Security.Claims;

namespace TkdFront.Pages
{
    public class TechniquesModel : PageModel
    {
        protected readonly IApiClient _apiClient;

        public TechniquesModel(IApiClient apiClient)
        {
            _apiClient = apiClient;
        }

        public List<TechniqueResponse> Techniques { get; set; }
        public List<ImageResponse> Images { get; set; }

        public bool IsAdmin { get; set; }
        public async Task OnGetAsync()
        {
            IsAdmin = User.IsAdmin();
            Techniques = await _apiClient.GetTechniquesAsync();
            Images = await _apiClient.GetImagesAsync();
        }
    }
}
