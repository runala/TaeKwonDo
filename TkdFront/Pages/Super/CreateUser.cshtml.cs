﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using TkdFront.Data;
using TkdFront.Services;

using System.Security.Claims;

namespace TkdFront.Pages.Super
{
    public class CreateUserModel : PageModel
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly ILogger<CreateUserModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly IAdminService _adminService;

        public CreateUserModel(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            ILogger<CreateUserModel> logger,
            IEmailSender emailSender,
            IAdminService adminService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _adminService = adminService;
        }
        public bool IsAdmin { get; set; }
        public bool IsSuper { get; set; }
        [BindProperty]
        public InputModel Input { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            [Required]
            [Display(Name = "Set Admin")]
            public bool SetAdmin { get; set; }
        }
        public async Task OnGetAsync()
        {
            IsAdmin = User.IsAdmin();
            IsSuper = User.IsSuper();
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();

        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                var user = new User { UserName = Input.Email, Email = Input.Email };
                if (User.IsSuper() && Input.SetAdmin)
                {
                    user.IsAdmin = true;
                }

                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                    if (user.IsAdmin)
                    {
                        _logger.LogInformation("Admin user created a new account with password.");
                    }
                    else
                    {
                        _logger.LogInformation("User created a new account with password.");
                    }
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return RedirectToPage();
        }
    }
}