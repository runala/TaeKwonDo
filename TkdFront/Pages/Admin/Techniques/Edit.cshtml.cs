﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using TkdDTO;
using TkdFront.Services;

namespace TkdFront.Pages.Admin.Techniques
{
    public class EditModel : PageModel
    {
        private readonly IApiClient _apiClient;

        public EditModel(IApiClient apiClient)
        {
            _apiClient = apiClient;
        }

        [BindProperty]
        public Technique Technique { get; set; }


        public async Task OnGetAsync(int id)
        {
            Technique = await _apiClient.GetTechniqueAsync(id);
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await _apiClient.PutTechniqueAsync(Technique.Tid, Technique);

            return RedirectToPage();
        }
        public async Task<IActionResult> OnPostDeleteAsync()
        {
            var delete = await _apiClient.GetTechniqueAsync(Technique.Tid);

            if (delete != null)
            {
                await _apiClient.DeleteTechniqueAsync(Technique.Tid);
            }

            return RedirectToPage("/Techniques/Index");
        }
    }
}