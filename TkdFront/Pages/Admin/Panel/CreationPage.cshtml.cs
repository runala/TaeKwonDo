using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using TkdDTO;
using TkdFront.Services;

namespace TkdFront.Pages.Admin
{
    public class CreationPageModel : PageModel
    {

        private readonly IApiClient _apiClient;

        public CreationPageModel(IApiClient apiClient)
        {
            _apiClient = apiClient;
        }

        [BindProperty]
        public Word Word { get; set; }

        [BindProperty]
        public Technique Technique { get; set; }

        [BindProperty]
        public Language Language { get; set; }

        [BindProperty]
        public Image Image { get; set; }

        public async Task<IActionResult> OnPostWordAsync()
        {
            var response = await _apiClient.PostWordAsync(Word);
            //return RedirectToPage($"EditWord", new { id = Word.Wid });
            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostTechniueAsync()
        {
            var response = await _apiClient.PostTechniqueAsync(Technique);
            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostLanguageAsync()
        {
            var response = await _apiClient.PostLanguageAsync(Language);
            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostImageAsync()
        {
            var response = await _apiClient.PostImageAsync(Image);

            return RedirectToPage();
        }
    }
}
