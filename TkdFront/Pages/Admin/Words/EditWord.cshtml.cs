using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Caching.Memory;
using TkdDTO;
using TkdFront.Services;

namespace TkdFront.Pages.Admin.Words
{
    public class EditWordModel : PageModel
    {
        private readonly IApiClient _apiClient;

        public EditWordModel(IApiClient apiClient)
        {
            _apiClient = apiClient;
            
        }

        [BindProperty]
        public List<WordResponse> Words { get; set; }

        
        [BindProperty]
        public Word Word { get; set; }
        public async Task OnGetAsync(int id)
        {
            Words = await _apiClient.GetWordAsync(id);
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await _apiClient.PutWordAsync(Word);

            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostDeleteAsync()
        {
            var delete = await _apiClient.GetWordAsync(Word.Wid, Word.Lcode);

            if (delete != null)
            {
                await _apiClient.DeleteWordAsync(Word.Wid, Word.Lcode);
            }

            return RedirectToPage("/Words/Index");
            
        }

    }
}
