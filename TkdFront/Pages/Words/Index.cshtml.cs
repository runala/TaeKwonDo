using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using TkdDTO;
using TkdFront.Services;
using System.Security.Claims;

namespace TkdFront.Pages
{
    public class WordsModel : PageModel
    {
        protected readonly IApiClient _apiClient;


        public WordsModel(IApiClient apiClient)
        {
            _apiClient = apiClient;
        }
        public List<ListWordResponse> Words { get; set; }
        public bool IsAdmin { get; set; }
        public async Task OnGetAsync()
        {
            IsAdmin = User.IsAdmin();
            Words = await _apiClient.GetWordsAsync();
        }
    }
}
