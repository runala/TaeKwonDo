﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using TkdDTO;

namespace TkdFront.Services
{
    public class ApiClient : IApiClient
    {
        private readonly HttpClient _httpClient;

        public ApiClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<LanguageResponse> GetLanguageAsync(string lcode)
        {
            if (string.IsNullOrEmpty(lcode))
            {
                return null;
            }
            var response = await _httpClient.GetAsync($"/api/languages/{lcode}");

            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                return null;
            }

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<LanguageResponse>();
        }

        public async Task<List<LanguageResponse>> GetLanguagesAsync()
        {
            var response = await _httpClient.GetAsync($"/api/languages");

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<List<LanguageResponse>>();
        }

        public async Task<TechniqueResponse> GetTechniqueAsync(int id)
        {

            var response = await _httpClient.GetAsync($"/api/techniques/{id}");

            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                return null;
            }

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<TechniqueResponse>();
        }

        public async Task<List<TechniqueResponse>> GetTechniquesAsync()
        {
            var response = await _httpClient.GetAsync($"/api/techniques");

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<List<TechniqueResponse>>();
            
        }

        public async Task<List<WordResponse>> GetWordAsync(int id)
        {

            var response = await _httpClient.GetAsync($"/api/words/{id}");

            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                return null;
            }

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<List<WordResponse>>();
        }

        public async Task<List<ListWordResponse>> GetWordsAsync()
        {
            var response = await _httpClient.GetAsync($"/api/words");

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<List<ListWordResponse>>();
        }

        public async Task<WordResponse> GetWordAsync(int id, string lcode)
        {
            var response = await _httpClient.GetAsync($"/api/words/{id}/{lcode}");

            if (response.StatusCode == HttpStatusCode.NotFound) return null;

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<WordResponse>();

        }

        public async Task<WordResponse> PostWordAsync(Word word)
        {
            var response = await _httpClient.PostAsJsonAsync($"/api/words", word);

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<WordResponse>();

        }

        public async Task PutWordAsync(Word word)
        {
            var response = await _httpClient.PutAsJsonAsync($"/api/words/{word.Wid}/{word.Lcode}", word);

            response.EnsureSuccessStatusCode();
        }

        public async Task DeleteWordAsync(int id, string lcode)
        {
            var response = await _httpClient.DeleteAsync($"/api/words/{id}/{lcode}");

            response.EnsureSuccessStatusCode();

        }

        public async Task<TechniqueResponse> PostTechniqueAsync(Technique technique)
        {
            var response = await _httpClient.PostAsJsonAsync($"/api/techniques", technique);

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<TechniqueResponse>();

        }

        public async Task PutTechniqueAsync(int id, Technique technique)
        {
            var response = await _httpClient.PutAsJsonAsync($"/api/techniques/{id}", technique);

            response.EnsureSuccessStatusCode();

        }

        public async Task DeleteTechniqueAsync(int id)
        {
            var response = await _httpClient.DeleteAsync($"/api/techniques/{id}");

            response.EnsureSuccessStatusCode();
        }

        public async Task<LanguageResponse> PostLanguageAsync(Language language)
        {
            var response = await _httpClient.PostAsJsonAsync($"/api/languages", language);

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<LanguageResponse>();
           
        }

        public async Task PutLanguageAsync(string lcode, Language language)
        {
            var response = await _httpClient.PutAsJsonAsync($"/api/languages/{lcode}", language);
            response.EnsureSuccessStatusCode();
        }

        public async Task DeleteLanguageAsync(string lcode)
        {
            var response = await _httpClient.DeleteAsync($"/api/languages/{lcode}");

            response.EnsureSuccessStatusCode();

        }

        public async Task<bool> CheckHealtAsync()
        {
            try
            {
                var response = await _httpClient.GetStringAsync("/healt");

                return string.Equals(response, "Healthy", StringComparison.OrdinalIgnoreCase);
            }
            catch
            {
                return false;
            }
        }

        public async Task<List<ImageResponse>> GetImagesAsync()
        {
            var response = await _httpClient.GetAsync($"/api/Images");

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<List<ImageResponse>>();

        }

        public async Task<List<ImageResponse>> GetImagesAsync(int id)
        {
            var response = await _httpClient.GetAsync($"/api/Images/{id}");

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<List<ImageResponse>>();
        }

        public async Task<ImageResponse> PostImageAsync(Image image)
        {
            var response = await _httpClient.PostAsJsonAsync($"/api/images", image);

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<ImageResponse>();
        }
    }
}
