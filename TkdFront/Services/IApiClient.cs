﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TkdDTO;


namespace TkdFront.Services
{
    public interface IApiClient
    {
        Task<List<LanguageResponse>> GetLanguagesAsync();
        Task<LanguageResponse> GetLanguageAsync(string lcode);
        Task<List<ListWordResponse>> GetWordsAsync();
        Task<List<WordResponse>> GetWordAsync(int id);
        Task<WordResponse> GetWordAsync(int id, string lcode);
        Task<List<TechniqueResponse>> GetTechniquesAsync();
        Task<TechniqueResponse> GetTechniqueAsync(int id);

        
        Task<WordResponse> PostWordAsync(Word word);
        Task PutWordAsync(Word word);
        Task DeleteWordAsync(int id, string lcode);


        Task<TechniqueResponse> PostTechniqueAsync(Technique technique);
        Task PutTechniqueAsync(int id, Technique technique);
        Task DeleteTechniqueAsync(int id);

        Task<LanguageResponse> PostLanguageAsync(Language language);
        Task PutLanguageAsync(string lcode, Language language);
        Task DeleteLanguageAsync(string lcode);

        Task<List<ImageResponse>> GetImagesAsync();
        Task<List<ImageResponse>> GetImagesAsync(int id);

        Task<ImageResponse> PostImageAsync(Image image);

        Task<bool>  CheckHealtAsync();
    }
}
