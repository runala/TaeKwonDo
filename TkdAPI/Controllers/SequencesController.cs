﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TkdAPI.Data;
using TkdDTO;

namespace TkdAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SequencesController : ControllerBase
    {
        private readonly ApplicationDbContext _db;

        public SequencesController(ApplicationDbContext db)
        {
            _db = db;
        }
    }
}
