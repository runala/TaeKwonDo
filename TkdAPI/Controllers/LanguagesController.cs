﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TkdAPI.Data;
using TkdDTO;

namespace TkdAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LanguagesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public LanguagesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Languages
        [HttpGet]
        public async Task<ActionResult<List<TkdDTO.LanguageResponse>>> GetLanguages()
        {
            var languages = await _context.Languages.AsNoTracking()
                                                    .Select( l => l.MapLanguageResponse())
                                                    .ToListAsync();

            return languages;
        }

        // GET: api/Languages/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TkdDTO.LanguageResponse>> GetLanguage(string id)
        {
            var language = await _context.Languages.AsNoTracking()
                                                .FirstOrDefaultAsync(l => l.Lcode == id);

            if (language == null)
            {
                return NotFound();
            }

            return language.MapLanguageResponse();
        }

// Post (Create)

        [HttpPost]
        public async Task<ActionResult<LanguageResponse>> Post(TkdDTO.Language input){

            var language = new Data.Language{
                Lcode = input.Lcode,
                Name = input.Name
            };

            _context.Languages.Add(language);
            await _context.SaveChangesAsync();

            var result = language.MapLanguageResponse();

            return CreatedAtAction(nameof(GetLanguage), new {id = result.Lcode}, result);
        }

        // Put (Update)

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(string id, TkdDTO.Language input){
            var language = await _context.Languages.FindAsync(id);

            if (language == null){
                return NotFound();
            }

            language.Lcode = input.Lcode;
            language.Name = input.Name;

            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<LanguageResponse>> Delete(string id){

            var language = await _context.Languages.FindAsync(id);

            if (language == null){
                return NotFound();
            }
            _context.Languages.Remove(language);
            await _context.SaveChangesAsync();

            return language.MapLanguageResponse();
        }
    }
}
