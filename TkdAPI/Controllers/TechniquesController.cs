﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TkdAPI.Data;
using TkdDTO;

namespace TkdAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TechniquesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public TechniquesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Techniques
        [HttpGet]
        public async Task<ActionResult<List<TkdDTO.TechniqueResponse>>> GetTechniques()
        {
            var techniques = await _context.Techniques.AsNoTracking()
                                                            .Include(t => t.Word)
                                                            .Select(t => t.MapTechniqueResponse())
                                                            .ToListAsync();

            return techniques;
        }

        // GET: api/Techniques/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TkdDTO.TechniqueResponse>> GetTechnique(int id)
        {
            var technique = await _context.Techniques.AsNoTracking()
                                                            .Include(t => t.Word)
                                                            .FirstOrDefaultAsync(t => t.Tid == id);

            if(technique == null)
            {
                return NotFound();
            }
            return technique.MapTechniqueResponse();
        }

        [HttpPost]
        public async Task<ActionResult<TechniqueResponse>> Post(TkdDTO.Technique input){
            var technique = new Data.Technique{
                
                Tid = input.Tid,
                Wid = input.Wid,
                Tdescription = input.Tdescription,
                Lcode = input.Lcode
            };

            _context.Techniques.Add(technique);

            await _context.SaveChangesAsync();

            var result = technique.MapTechniqueResponse();

            return CreatedAtAction(nameof(GetTechnique), new {id = result.Tid}, result);

        }

         [HttpPut("{id}")]
         public async Task<IActionResult> Put(int id, TkdDTO.Technique input){

             var technique = await _context.Techniques.FindAsync(id);

             if (technique == null){
                 return NotFound();
             } 

             technique.Tdescription = input.Tdescription;

             await _context.SaveChangesAsync();

             return NoContent();
         }

        [HttpDelete("{id}")]
        public async Task<ActionResult<TechniqueResponse>> Delete(int id){
            
            var technique = await _context.Techniques.FindAsync(id);

            if(technique == null) return NotFound();

            _context.Techniques.Remove(technique);
            await _context.SaveChangesAsync();

            return technique.MapTechniqueResponse();

        }

        
    }
}
