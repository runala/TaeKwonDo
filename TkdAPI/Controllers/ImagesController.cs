﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TkdAPI.Data;
using TkdDTO;
namespace TkdAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImagesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ImagesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Images
        [HttpGet]
        public async Task<ActionResult<List<TkdDTO.ImageResponse>>> GetImages()
        {
            var images = await _context.Images.AsNoTracking()
                                                .Select(i => i.MapImageResponse())
                                                .ToListAsync();
            return images;
        }

        // GET: api/Images/5
        [HttpGet("{id}")]
        public async Task<ActionResult<List<TkdDTO.ImageResponse>>> GetImage(int id)
        {
            var images = await _context.Images.AsNoTracking()
                                               .Where(i => i.Iid == id)
                                               .Select(i => i.MapImageResponse())
                                               .ToListAsync();

            if (images == null)
            {
                return NotFound();
            }

            return images;
        }

        [HttpPost]
        public async Task<ActionResult<ImageResponse>> PostImage(TkdDTO.Image input)
        {
            var image = new Data.Image
            {
                Iid = input.Iid,
                Link = input.Link
            };

            _context.Images.Add(image);
            await _context.SaveChangesAsync();

            var result = image.MapImageResponse();

            return CreatedAtAction("GetImage", new { id = result.Iid }, result);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, TkdDTO.Image input)
        {
            var image = await _context.Images.FindAsync(id);

            if (image == null)
            {
                return NotFound();
            }

            image.Link = input.Link;

            await _context.SaveChangesAsync();

            return NoContent();
        }

        // DELETE: api/Images/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ImageResponse>> DeleteImage(int id)
        {
            var image = await _context.Images.FindAsync(id);
            if (image == null)
            {
                return NotFound();
            }

            _context.Images.Remove(image);
            await _context.SaveChangesAsync();

            return NoContent();
        }

    }

}
