﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TkdAPI.Data;
using TkdDTO;

namespace TkdAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WordsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public WordsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Words
        [HttpGet]
        public async Task<ActionResult<List<TkdDTO.ListWordResponse>>> GetWords()
        {
            List<ListWordResponse> responses = new List<ListWordResponse>();

            var words = await _context.Words.AsNoTracking()
                                               .Select(w => w.MapMultiWordResponse())
                                               .ToListAsync();

            var images = await _context.WordImages.AsNoTracking()
                                                    .Include(i => i.Image)
                                                    .Select(wi => wi.MapWordImageResponse())
                                                    .ToListAsync();

            var ids = words.Select(w => w.Wid).Distinct().ToList();


            foreach(int i in ids)
            {
                responses.Add(new ListWordResponse
                {
                    Words = words.Where(w => w.Wid == i).ToList(),
                    WordImages = images.Where(im => im.Wid == i).ToList()
                });
            }
           
            return responses;
        }

        // GET: api/Words/5
        [HttpGet("{id}")]
        public async Task<ActionResult<List<TkdDTO.WordResponse>>> GetWord(int id)
        {
            var word = await _context.Words.AsNoTracking()
                                           .Where(w => w.Wid == id)
                                           .Select(w => w.MapSingelWordResponse())
                                           .ToListAsync();
            if (word == null)
            {
                return NotFound();
            }

            return word;
        }
        [HttpGet("{id}/{lcode}")]
        public async Task<ActionResult<TkdDTO.WordResponse>> GetWord(int id, string lcode)
        {
            var word = await _context.Words.Include(w => w.WordImage).ThenInclude(w => w.Image).FirstOrDefaultAsync(w => w.Wid == id && w.Lcode == lcode);
            
            if (word == null)
            {
                return NotFound();
            }

            var result = word.MapSingelWordResponse();

            return result;
        }

        // Post (Create)

        [HttpPost]
        public async Task<ActionResult<WordResponse>> Post(TkdDTO.Word input)
        {

            var word = new Data.Word
            {
                Wid = input.Wid,
                Wterm = input.Wterm,
                Lcode = input.Lcode
            };

            _context.Words.Add(word);
            await _context.SaveChangesAsync();

            var result = word.MapSingelWordResponse();

            return CreatedAtAction(nameof(GetWord), new { id = result.Wid }, result);
        }

        // Put (Update)

        [HttpPut("{id}/{lcode}")]
        public async Task<IActionResult> Put(int id, string lcode, TkdDTO.Word input)
        {
            var word = await _context.Words.FindAsync(id, lcode);

            if (word == null)
            {
                return NotFound();
            }

            word.Wterm = input.Wterm;

            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{id}/{lcode}")]
        public async Task<ActionResult<WordResponse>> Delete(int id, string lcode)
        {

            var word = await _context.Words.FindAsync(id, lcode);

            if (word == null)
            {
                return NotFound();
            }
            _context.Words.Remove(word);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpGet("/Image{id}")]
        public async Task<List<WordImageResponse>> GetWordImagesAsync(int id)
        {
            var image = await _context.WordImages.Where(wi => wi.Wid == id).Include(wi => wi.Image).Select(wi => wi.MapWordImageResponse()).ToListAsync();

            return image;
        }

        [HttpPost("/Image")]
        public async Task<ActionResult<WordImageResponse>> PostImage(TkdDTO.WordImage input)
        {
            var image = new Data.WordImage
            {
                Iid = input.Iid,
                Wid = input.Wid,
                Lcode = input.Lcode,
                Order = input.Order
            };

            _context.WordImages.Add(image);

            await _context.SaveChangesAsync();

            var res = image.MapWordImageResponse();

            return res;
        }
    }
}


