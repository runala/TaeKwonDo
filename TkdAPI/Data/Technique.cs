﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TkdAPI.Data
{
    public class Technique : TkdDTO.Technique
    {
        public Word Word { get; set; }
    }
}
