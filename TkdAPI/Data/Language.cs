﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TkdAPI.Data
{
    public class Language : TkdDTO.Language
    {
        public ICollection<Word> Words { get; set; }
    }
}
