﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TkdAPI.Data
{
    public class Sequence : TkdDTO.Sequence
    {
        public SequenceImage SequenceImage { get; set; }
    }
}
