﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TkdAPI.Data
{
    public class Word : TkdDTO.Word
    {
        public Language Language { get; set; }
        public Technique Technique { get; set; }
        public ICollection<WordImage> WordImage { get; set; }
    }
}
