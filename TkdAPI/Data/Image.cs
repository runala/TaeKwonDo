﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TkdAPI.Data
{
    public class Image : TkdDTO.Image
    {
        public ICollection<WordImage> WordImage { get; set; }
        public ICollection<SequenceImage> SequenceImage { get; set; }
    }
}
