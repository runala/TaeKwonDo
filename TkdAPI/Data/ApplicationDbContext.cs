﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TkdAPI.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Language>()
                .HasKey(a => a.Lcode);

            modelBuilder.Entity<Word>()
                .HasKey(w => new { w.Wid, w.Lcode });

            modelBuilder.Entity<Language>().HasMany<Word>(l => l.Words)
                .WithOne(w => w.Language).HasForeignKey(w => w.Lcode);

            modelBuilder.Entity<Technique>(entity =>
            {
                entity.HasKey(e => e.Tid);
                entity.HasOne(t => t.Word)
                .WithOne(w => w.Technique)
                .HasForeignKey<Technique>(t => new { t.Wid, t.Lcode });
            });

            modelBuilder.Entity<Sequence>().HasKey(s => s.Sid);

            modelBuilder.Entity<Image>().HasKey(i => i.Iid);

            modelBuilder.Entity<WordImage>(entity =>
            {
                entity.HasKey(wi => new { wi.Iid, wi.Wid });
                entity.HasOne(wi => wi.Image)
                .WithMany(i => i.WordImage)
                .HasForeignKey(wi => wi.Iid);
                entity.HasOne(wi => wi.Word)
                .WithMany(w => w.WordImage)
                .HasForeignKey(wi => new { wi.Wid, wi.Lcode });
            });


            modelBuilder.Entity<SequenceImage>(entity =>
            {
                entity.HasKey(si => new { si.Sid, si.Iid });
                entity.HasOne(si => si.Image)
                .WithMany(i => i.SequenceImage)
                .HasForeignKey(si => si.Iid);
                entity.HasOne(si => si.Sequence)
                .WithOne(s => s.SequenceImage)
                .HasForeignKey<SequenceImage>(si => si.Sid);
            });
            
        }

        public DbSet<Word> Words { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Technique> Techniques { get; set; }
        public DbSet<Sequence> Sequences { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<WordImage> WordImages { get; set; }
        public DbSet<SequenceImage> SequenceImages { get; set; }
    }
}
