﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TkdAPI.Data
{
    public class WordImage : TkdDTO.WordImage
    {
        public Image Image { get; set; }
        public Word Word { get; set; }
    }
}
