﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TkdAPI.Data
{
    public class SequenceImage : TkdDTO.SequenceImage
    {
        public Sequence Sequence { get; set; }
        public Image Image { get; set; }
    }
}
