﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TkdAPI.Data;
using TkdDTO;

namespace TkdAPI.Data
{
    public static class EntityExtensions
    {
        public static TkdDTO.LanguageResponse MapLanguageResponse(this Language language) =>
            new TkdDTO.LanguageResponse
            {
                Lcode = language.Lcode,
                Name = language.Name
            };
        //Map return for singel word requests
        public static TkdDTO.WordResponse MapSingelWordResponse(this Word word) =>
            new TkdDTO.WordResponse
            {
                Wid = word.Wid,
                Wterm = word.Wterm,
                Lcode = word.Lcode,
                WordImages = word.WordImage?
                .Select( wi => new TkdDTO.WordImageResponse
                {
                    Iid = wi.Iid,
                    Wid = wi.Wid,
                    Order = wi.Order,
                    Image = new TkdDTO.ImageResponse
                    {
                        Iid = wi.Iid,
                        Link = wi.Image?.Link
                    }
                    
                }).ToList()
            };
        public static TkdDTO.WordsResponse MapMultiWordResponse(this Word word) =>
            new TkdDTO.WordsResponse
            {
                Wid = word.Wid,
                Wterm = word.Wterm,
                Lcode = word.Lcode
            };

        public static TkdDTO.TechniqueResponse MapTechniqueResponse(this Technique technique) =>
            new TkdDTO.TechniqueResponse
            {
                Tid = technique.Tid,
                Wid = technique.Wid,
                Lcode = technique.Lcode,
                Tdescription = technique.Tdescription,
                Word = new TkdDTO.WordResponse
                {
                    Wid = technique?.Wid ?? 0,
                    Lcode = technique?.Lcode,
                    Wterm = technique.Word?.Wterm , 
                }
            };

        public static TkdDTO.ImageResponse MapImageResponse(this Image image) =>
            new TkdDTO.ImageResponse
            {
                Iid = image.Iid,
                Link = image.Link
            };

        public static TkdDTO.WordImageResponse MapWordImageResponse(this WordImage wordImage) =>
            new TkdDTO.WordImageResponse
            {
                Iid = wordImage.Iid,
                Wid = wordImage.Wid,
                Order = wordImage.Order,
                Image = new TkdDTO.ImageResponse
                {
                    Iid = wordImage?.Iid ?? 0,
                    Link = wordImage.Image?.Link
                }
            };
    }
}
