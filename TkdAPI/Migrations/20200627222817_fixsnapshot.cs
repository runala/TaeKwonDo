﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TkdAPI.Migrations
{
    public partial class fixsnapshot : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Images",
                columns: table => new
                {
                    Iid = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Link = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Images", x => x.Iid);
                });

            migrationBuilder.CreateTable(
                name: "Languages",
                columns: table => new
                {
                    Lcode = table.Column<string>(maxLength: 3, nullable: false),
                    Name = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Languages", x => x.Lcode);
                });

            migrationBuilder.CreateTable(
                name: "Sequences",
                columns: table => new
                {
                    Sid = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sequences", x => x.Sid);
                });

            migrationBuilder.CreateTable(
                name: "Words",
                columns: table => new
                {
                    Wid = table.Column<int>(nullable: false),
                    Lcode = table.Column<string>(nullable: false),
                    Wterm = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Words", x => new { x.Wid, x.Lcode });
                    table.ForeignKey(
                        name: "FK_Words_Languages_Lcode",
                        column: x => x.Lcode,
                        principalTable: "Languages",
                        principalColumn: "Lcode",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SequenceImages",
                columns: table => new
                {
                    Iid = table.Column<int>(nullable: false),
                    Sid = table.Column<int>(nullable: false),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SequenceImages", x => new { x.Sid, x.Iid });
                    table.ForeignKey(
                        name: "FK_SequenceImages_Images_Iid",
                        column: x => x.Iid,
                        principalTable: "Images",
                        principalColumn: "Iid",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SequenceImages_Sequences_Sid",
                        column: x => x.Sid,
                        principalTable: "Sequences",
                        principalColumn: "Sid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Techniques",
                columns: table => new
                {
                    Tid = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Wid = table.Column<int>(nullable: false),
                    Lcode = table.Column<string>(nullable: true),
                    Tdescription = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Techniques", x => x.Tid);
                    table.ForeignKey(
                        name: "FK_Techniques_Words_Wid_Lcode",
                        columns: x => new { x.Wid, x.Lcode },
                        principalTable: "Words",
                        principalColumns: new[] { "Wid", "Lcode" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WordImages",
                columns: table => new
                {
                    Iid = table.Column<int>(nullable: false),
                    Wid = table.Column<int>(nullable: false),
                    Lcode = table.Column<string>(nullable: true),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WordImages", x => new { x.Iid, x.Wid });
                    table.ForeignKey(
                        name: "FK_WordImages_Images_Iid",
                        column: x => x.Iid,
                        principalTable: "Images",
                        principalColumn: "Iid",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WordImages_Words_Wid_Lcode",
                        columns: x => new { x.Wid, x.Lcode },
                        principalTable: "Words",
                        principalColumns: new[] { "Wid", "Lcode" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SequenceImages_Iid",
                table: "SequenceImages",
                column: "Iid");

            migrationBuilder.CreateIndex(
                name: "IX_SequenceImages_Sid",
                table: "SequenceImages",
                column: "Sid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Techniques_Wid_Lcode",
                table: "Techniques",
                columns: new[] { "Wid", "Lcode" },
                unique: true,
                filter: "[Lcode] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_WordImages_Wid_Lcode",
                table: "WordImages",
                columns: new[] { "Wid", "Lcode" });

            migrationBuilder.CreateIndex(
                name: "IX_Words_Lcode",
                table: "Words",
                column: "Lcode");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SequenceImages");

            migrationBuilder.DropTable(
                name: "Techniques");

            migrationBuilder.DropTable(
                name: "WordImages");

            migrationBuilder.DropTable(
                name: "Sequences");

            migrationBuilder.DropTable(
                name: "Images");

            migrationBuilder.DropTable(
                name: "Words");

            migrationBuilder.DropTable(
                name: "Languages");
        }
    }
}
