﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TkdDTO
{
    public class Sequence
    {
        // Løpenummer
        public int Sid { get; set; }
        // t.d Chong-ji
        public string Title { get; set; }
        // Forklaring + Fokus punkt
        public string Description { get; set; }
        // Sequnce type = mønster, avtalt kamp
        public int Type { get; set; }
    }
}
