﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TkdDTO
{
    public class Language
    {
        [Key]
        [StringLength(3)]
        public string Lcode { get; set; }

        [StringLength(20)]
        public string Name { get; set; }
    }
}
