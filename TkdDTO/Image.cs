﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TkdDTO
{
    public class Image
    {
        [Key]
        public int Iid { get; set; }
        public string Link { get; set; }
    }
}
