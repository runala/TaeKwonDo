﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TkdDTO
{
    public class TechniqueResponse : Technique
    {
        public Word Word { get; set; }
    }
}
