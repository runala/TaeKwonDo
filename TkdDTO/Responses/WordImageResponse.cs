﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TkdDTO
{
    public class WordImageResponse
    {
        public int Iid { get; set; }
        public int Wid { get; set; }
        public int Order { get; set; }
        public ImageResponse Image { get; set; }
    }
}
