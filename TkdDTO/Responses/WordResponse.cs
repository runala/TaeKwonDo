﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TkdDTO
{
    public class WordResponse : Word
    {
        public ICollection<WordImageResponse> WordImages { get; set; }

    }
}
