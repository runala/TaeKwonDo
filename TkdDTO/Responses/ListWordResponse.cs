﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TkdDTO
{
    public class ListWordResponse
    {
        public ICollection<WordsResponse> Words { get; set; }
        public ICollection<WordImageResponse> WordImages { get; set; }
    }
}
