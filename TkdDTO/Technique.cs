﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TkdDTO
{
    public class Technique
    {
        [Key]
        public int Tid { get; set; }

        public int Wid { get; set; }
        public string Lcode { get; set; }
        [StringLength(500)]
        public string Tdescription { get; set; }
    }
}
