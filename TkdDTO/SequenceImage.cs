﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TkdDTO
{
    public class SequenceImage
    {
        public int Iid { get; set; }
        public int Sid { get; set; }
        public int Order { get; set; }
    }
}
