﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TkdDTO
{
    public class Word
    {
        
        public int Wid { get; set; }
        
        public string Lcode { get; set; }

        [Required]
        [StringLength(50)]
        public string Wterm { get; set; }
    }
}
