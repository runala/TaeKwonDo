﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TkdDTO
{
    public class WordImage
    {
        // FK Image table
        public int Iid { get; set; }
        // FK Word table
        public int Wid { get; set; }
        // FK Word table
        public string Lcode { get; set; }
        // for sortering av bilder
        public int Order { get; set; }
    }
}
